export function adjustColor(color: string, percent: number) {
  if (color === 'transparent') return 'transparent';
  const adjustedColor = color.match(
    /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i
  );
  if (!adjustedColor) {
    throw new Error('Invalid color format. Please use the format #FFFFFF.');
  }

  const [, r, g, b] = adjustedColor;
  const [red, green, blue] = [r, g, b].map((hex) => parseInt(hex, 16));

  const adjustedRed = Math.max(
    Math.min(Math.round((red * (100 + percent)) / 100), 255),
    0
  );
  const adjustedGreen = Math.max(
    Math.min(Math.round((green * (100 + percent)) / 100), 255),
    0
  );
  const adjustedBlue = Math.max(
    Math.min(Math.round((blue * (100 + percent)) / 100), 255),
    0
  );

  return `#${adjustedRed.toString(16).padStart(2, '0')}${adjustedGreen.toString(16).padStart(2, '0')}${adjustedBlue.toString(16).padStart(2, '0')}`;
}
