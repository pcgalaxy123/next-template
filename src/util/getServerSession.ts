import { authOptions } from '@/config/next-auth';
import { getServerSession as gss } from 'next-auth';

export const getServerSession = () => gss(authOptions);
