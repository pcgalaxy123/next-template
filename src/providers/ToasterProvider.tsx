import { Toaster } from 'sonner';

export const ToasterProvider = () => {
  return <Toaster closeButton richColors />;
};
