'use client';

import { ToasterProvider } from './ToasterProvider';
import { store } from '@/store';
import { SessionProvider } from 'next-auth/react';
import { PropsWithChildren } from 'react';
import { Provider as StoreProvider } from 'react-redux';

export const Providers = ({ children }: PropsWithChildren) => {
  return (
      <StoreProvider store={store}>
        <SessionProvider>{children}</SessionProvider>
        <ToasterProvider />
      </StoreProvider>
  );
};
