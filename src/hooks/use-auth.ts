import { UseSessionOptions, useSession } from 'next-auth/react';
import { signIn, signOut } from 'next-auth/react';

export const useAuth = <R extends boolean>(options?: UseSessionOptions<R>) => {
  const { data, status } = useSession(options);

  return {
    session: data,
    status,
    signIn,
    signOut,
  };
};
