import { PropsWithChildren } from 'react';

import { Providers } from '@/providers';

export default function Layout({ children }: PropsWithChildren) {
  return (
    <html suppressHydrationWarning>
      <head></head>
      <body>
        <Providers>
          <Navbar height='50px' />
          <main>
            {children}
          </main>
        </Providers>
      </body>
    </html>
  );
}
