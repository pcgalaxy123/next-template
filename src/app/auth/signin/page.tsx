import { getServerSession } from '@/util/getServerSession';
import { getProviders } from 'next-auth/react';
import { redirect } from 'next/navigation';

import SignIn from '@/components/modules/SignIn';

export default async function Login() {
  const providers = await getProviders();
  const session = await getServerSession();

  if (session?.user) {
    redirect('/');
  }

  return <SignIn providers={providers} />;
}
