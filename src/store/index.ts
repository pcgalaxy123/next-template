import { postsApi } from './api/posts';
import { countSlice } from './slices/count';
import { userSlice } from './slices/user';
import { combineReducers, configureStore } from '@reduxjs/toolkit';
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';

export const store = configureStore({
  reducer: combineReducers({
    [countSlice.reducerPath]: countSlice.reducer,
    [userSlice.reducerPath]: userSlice.reducer,
    [postsApi.reducerPath]: postsApi.reducer,
  }),
  middleware: (gdm) => gdm().concat(postsApi.middleware),
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export const useAppDispatch: () => AppDispatch = useDispatch;
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
