import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export type Post = {
  userId: number;
  id: number;
  title: string;
  body: string;
};

export const postsApi = createApi({
  reducerPath: 'postsApi',
  baseQuery: fetchBaseQuery({
    baseUrl: 'https://jsonplaceholder.typicode.com',
  }),
  endpoints: (build) => ({
    getPosts: build.query<Post[], void>({
      query: () => ({
        url: '/posts',
      }),
    }),
    getPost: build.query<Post, number>({
      query: (id) => ({
        url: `/posts/${id}`,
      }),
    }),
  }),
});
