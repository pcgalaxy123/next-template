import { PayloadAction, createSlice } from '@reduxjs/toolkit';

type CountState = {
  c: number;
};

export const countSlice = createSlice({
  name: 'counter',
  initialState: {
    c: 0,
  } as CountState,
  reducers: {
    increment: (state, { payload }: PayloadAction<number>) => {
      state.c = state.c + payload;
    },
    decrement: (state, { payload }: PayloadAction<number>) => {
      state.c = state.c - payload;
    },
  },
});

export const { increment, decrement } = countSlice.actions;
