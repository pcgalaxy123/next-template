import { PayloadAction, createSlice } from '@reduxjs/toolkit';

type UserState = {
  name: string;
  age?: number;
};

export const userSlice = createSlice({
  name: 'user',
  initialState: {
    name: '',
    age: undefined,
  } as UserState,
  reducers: {
    login: (state, { payload }: PayloadAction<[string, number]>) => {
      state.name = payload[0];
      state.age = payload[1];
    },
  },
});

export const { login } = userSlice.actions;
