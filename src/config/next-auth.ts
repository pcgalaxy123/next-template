import { prisma } from '@/config/prisma';
import { PrismaAdapter } from '@auth/prisma-adapter';
import { AuthOptions } from 'next-auth';
import { Adapter } from 'next-auth/adapters';
import Email from 'next-auth/providers/email';

export const authOptions: AuthOptions = {
  providers: [
    Email({
      server: {
        host: process.env.SMTP_HOST,
        port: '587',
        auth: {
          user: process.env.SMTP_USER,
          pass: process.env.SMTP_PASS,
        },
      },
      from: process.env.SMTP_FROM,
    }),
  ],
  adapter: PrismaAdapter(prisma) as Adapter,
};
